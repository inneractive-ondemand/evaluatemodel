package com.inneractive.eval

import java.io.{File, FileInputStream}
import java.util
import javax.xml.transform.stream.StreamSource

import org.dmg.pmml.{FieldName, Model, PMML}
import org.jpmml.evaluator._
import org.jpmml.model.JAXBUtil

import scala.collection.Map
import scala.collection.JavaConversions._

/**
  * Created by inneractive on 1/11/18.
  */
object Evaluateor extends App{

  val inputStream = new FileInputStream(new File("PMML.xml"))
  val pmmModel: PMML = JAXBUtil.unmarshalPMML(new StreamSource(inputStream))

  val evaluatorFactory: ModelEvaluatorFactory = ModelEvaluatorFactory.newInstance()
  val evaluator: ModelEvaluator[_ <: Model] = evaluatorFactory.newModelEvaluator(pmmModel)
  val activeFields: util.List[InputField] = evaluator.getActiveFields
  val targetFields: util.List[TargetField] = evaluator.getTargetFields
  val outputFields: util.List[OutputField] = evaluator.getOutputFields

  val states = Map("age"->18 ,"workclass"->"Private","fnlwgt"->309634,"education"->"11th","education_num"->7,"marital_status"->"Never-married","occupation"->"Other-service","relationship"->"Own-child","race"->"White","sex"->"Female","capital_gain"->0	,"capital_loss"->0	,"hours_per_week"->22	,"native_country"->"United-States")


  val pmmlarguments = new util.LinkedHashMap[FieldName, FieldValue]()

  activeFields.foreach {
    x => {
      val inputFieldName: FieldName = x.getName
      val rawValue = states.get(inputFieldName.getValue).get
      val activeValue: FieldValue = x.prepare(rawValue)
      pmmlarguments.put(inputFieldName, activeValue)
    }
  }

  val result = evaluator.evaluate(pmmlarguments)
  println(result.get(new FieldName("pmml(prediction)")))
  println(result.get(new FieldName("probability( <=50K)")))
  println(result.get(new FieldName("probability( >50K)")))
  println(result.get(new FieldName("prediction")))


  val pmmlarguments2 = new util.LinkedHashMap[FieldName, FieldValue]()

  val states2 = Map("age"->52 ,"workclass"->"Self-emp-not-inc","fnlwgt"->209642,"education"->"HS-grad","education_num"->9,"marital_status"->"Married-civ-spouse","occupation"->"Exec-managerial","relationship"->"Husband","race"->"White","sex"->"Male","capital_gain"->0	,"capital_loss"->0	,"hours_per_week"->45	,"native_country"->"United-States")

  activeFields.foreach {
    x => {
      val inputFieldName: FieldName = x.getName
      val rawValue = states2.get(inputFieldName.getValue).get
      val activeValue: FieldValue = x.prepare(rawValue)
      pmmlarguments2.put(inputFieldName, activeValue)
    }
  }


  val result2 = evaluator.evaluate(pmmlarguments2)
  println(result2.get(new FieldName("pmml(prediction)")))
  println(result2.get(new FieldName("probability( <=50K)")))
  println(result2.get(new FieldName("probability( >50K)")))
  println(result2.get(new FieldName("prediction")))

}

/* OUTPUT

 <=50K
0.5618893527705997
0.43811064722940035
0.0
 >50K
0.24444315934183136
0.7555568406581686
1.0

*/
